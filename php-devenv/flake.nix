{
  description = "Development environment for PHP based on devenv";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-unstable";
    devenv.url = "github:cachix/devenv";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = {
    self,
    nixpkgs,
    utils,
    devenv,
    ...
  } @ inputs:
    utils.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs {inherit system;};
    in rec {
      packages = utils.lib.flattenTree {
        devenv-up = devShell.config.procfileScript;
      };

      formatter = pkgs.alejandra;

      devShell = devenv.lib.mkShell {
        inherit inputs pkgs;
        modules = [
          ({
            pkgs,
            config,
            ...
          }: {
            languages.php = {
              enable = true;
              version = "8.2";

              extensions = [
                "xdebug"
              ];

              ini = ''
                display_errors = On
                error_reporting = E_ALL

                xdebug.mode = develop,debug
                xdebug.start_with_request = yes
                xdebug.output_dir = /dev/shm
              '';
            };
          })
        ];
      };
    });
}
