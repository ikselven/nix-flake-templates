{
  description = "A very basic flake with flake-utils";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = {
    self,
    utils,
    nixpkgs,
    ...
  } @ inputs:
    utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages.${system};
    in rec {
      formatter = pkgs.alejandra;

      packages = utils.lib.flattenTree {hello = pkgs.hello;};

      defaultPackage = packages.hello;

      #devShell = pkgs.mkShell { buildInputs = with pkgs; [ ]; };
    });
}
