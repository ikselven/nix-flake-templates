{
  description = "A basic flake for minikube, kubectl and friends";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = {
    self,
    utils,
    nixpkgs,
    ...
  } @ inputs:
    utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages.${system};
    in rec {
      formatter = pkgs.alejandra;
      devShell = pkgs.mkShell {
        shellHook = ''
          export MINIKUBE_HOME="''${PWD}/.minikube"
          export KUBECONFIG="''${PWD}/.kube/config"
        '';
        buildInputs = with pkgs; [
          kubectl
          kubernetes-helm
          minikube
        ];
      };
    });
}
