{
  description = "A basic flake for a PHP project using composer";

  inputs = {
    nixpkgs.url = "nixpkgs/nixpkgs-unstable";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = {
    self,
    utils,
    nixpkgs,
    ...
  } @ inputs:
    utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages.${system};
    in rec {
      formatter = pkgs.alejandra;
      # TODO: adapt PHP version if required
      devShell = pkgs.mkShell {
        buildInputs = with pkgs; [php] ++ (with pkgs.phpPackages; [composer]);
      };
    });
}
