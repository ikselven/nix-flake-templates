{
  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    utils.url = "github:numtide/flake-utils";
  };
  description = "nix flake templates";

  outputs = {
    self,
    nixpkgs,
    utils,
    ...
  } @ inputs:
    utils.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs {};
    in {
      formatter = pkgs.alejandra;
    })
    // {
      templates = {
        minikube = {
          path = ./minikube;
          description = "A basic flake for minikube, kubectl and friends";
        };

        php-composer = {
          path = ./php-composer;
          description = "A basic flake for a PHP project using composer";
        };

        php-devenv = {
          path = ./php-devenv;
          description = "Development environment for PHP based on devenv";
        };

        trivial-flake-utils = {
          path = ./trivial-flake-utils;
          description = "A very basic flake with flake-utils";
        };
      };

      defaultTemplate = self.templates.trivial-flake-utils;
    };
}
